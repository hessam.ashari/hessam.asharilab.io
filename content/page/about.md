---
title: About me
subtitle: راجع به من
comments: false
bigimg: [{src: "/img/cyber-security-programming-languages.jpg"}]
---
#### About me
______
<p style='direction:rtl; text-align: right'>
حسام اشعری هستم SysAdmin و برنامه نویس. <br>
علاقه مند به : <br>

- GNU/Linux </br>
- نرم افزار های آزاد </br>
- Arch Linux ❤️ </br>
- برنامه نویسی و به طور خاص bash </br>

<p style='direction:rtl; text-align: right'>
و البته کاربر xfce هستم. <br>
راه های ارتباطی هم که به طور مفصل در پایین وبلاگ ذکر شده. <br>
خوشحال میشم یه سر به گیت هابم بزنید و دو سه تا پروژه ای رو که دارم تست کنید.
______
{{< gallery caption-effect="fade" >}}
  {{< figure  link="/img/arch_screen1.png" caption="ArchLinux xfce" alt="Desktop Screenshot" >}}
  {{< figure  link="/img/arch_screen2.png" caption="ArchLinux xfce" alt="Desktop Screenshot" >}}
  {{< figure  link="/img/atom.png" caption="Atom Text Editor" alt="Programming workspace" >}}
  {{< figure  link="/img/vscode.png" caption="Visual Studio Code" alt="Programming workspace" >}}
  {{< figure  link="/img/room.jpg" alt="Programming workspace" >}}
  {{< figure  link="/img/arch_screen3.png" caption="screenfetch" >}}
{{< /gallery >}}
